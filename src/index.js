import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './styles.css';
import * as maptalks from 'maptalks';
var map;
export default class ReactMaptalk extends Component {
  static propTypes = {
    lat: PropTypes.number,
    lon: PropTypes.number,
    zoom: PropTypes.number,
    style:  PropTypes.string
  }

  componentDidMount(){
    const { lat, lon, zoom, style } = this.props;
        map = new maptalks.Map( this.mapContainer ,{
            center: [lon, lat],
            zoom: zoom,
            baseLayer: new maptalks.TileLayer('base', {
              urlTemplate: `https://{s}.basemaps.cartocdn.com/${style}_all/{z}/{x}/{y}.png`,
              subdomains: ['a','b','c','d'],
              attribution: '&copy; <a href="http://osm.org">OpenStreetMap</a> contributors, &copy; <a href="https://carto.com/">CARTO</a>'
            })
    });
  }

  render() {

    return (
        <div ref={el => this.mapContainer = el} className={styles.mapContainer}></div>
    )
  }
}
