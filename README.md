# react-maptalk

> maps lib

[![NPM](https://img.shields.io/npm/v/react-maptalk.svg)](https://www.npmjs.com/package/react-maptalk) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save react-maptalk
```

## Usage

```jsx
import React, { Component } from 'react'

import ReactMaptalk from 'react-maptalk'

class Example extends Component {
  render () {
    return (
      <ReactMaptalk 
          lat={28.5656} 
          lon={78.36666} 
          zoom={7} 
          style="dark" 
      />
    )
  }
}
```

## License

MIT © [anujsinghwd](https://github.com/anujsinghwd)
