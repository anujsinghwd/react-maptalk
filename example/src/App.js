import React, { Component } from 'react'

import ReactMaptalk from 'react-maptalk'

export default class App extends Component {
  render () {
    return (
        <ReactMaptalk 
          lat={28.5656} 
          lon={78.36666} 
          zoom={7} 
          style="dark" 
        />
    )
  }
}
